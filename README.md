# Jmustache

## 简介

jmustache 是mustache模板系统的零依赖实现，通过使用散列或对象中提供的值来扩展模板中的标签。

## 效果展示

<img src="./gif/video.gif"/>

## 下载安装

```
ohpm install mustache 
ohpm install @types/mustache --save-dev //import mustache 的时候语法报错.其原因是mustache包内不含类型声明,需要 @types/mustache 下载这个包的声明文件,从而解决语法的报错.

```
OpenHarmony ohpm 环境配置等更多内容，请参考[如何安装 OpenHarmony ohpm 包](https://gitee.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.md)

## 使用说明

解析方式主要使用`Mustache.render`方法

data：
以下大部分示例使用下方代码块数据，如有额外数据会单独列出

```
    name: "cai",
    msg: {
      sex: "male",
      age: "66",
      hobby: "reading"
    },
    focus: "<span>sleep<span>",
    subject: [" Ch ", " En ", " Math ", " physics "],
    moreInfo: []
```

### 变量

最基本的标签类型是一个简单的变量。`{{name}}`标签呈现`name`当前上下文中键的值。如果没有这样的键，则不会渲染任何内容。

默认情况下，所有变量都是 HTML 转义的。如果要呈现未转义的 HTML，请使用三重胡须：`{{{name}}}`。您还可以使用`&`取消转义变量。

如果您想全局更改 HTML 转义行为（例如，更改为模板非 HTML 格式），您可以覆盖 Mustache 的转义函数。例如，要禁用所有转义：`Mustache.escape = function(text) {return text;};`.

如果您`{{name}}` *不*希望被解释为 mustache 标记，而是希望`{{name}}`在输出中完全显示，则必须更改然后恢复默认分隔符。有关详细信息，请参阅自定义分隔符部分。

输入：

```
NAME:{{name}}
EMPTY:{{nothing}}
{{focus}}
{{{focus}}}
{{&focus}}
```

输出：

```
NAME:cai
EMPTY:
&lt;span&gt;sleep&lt;span&gt;
<span>sleep<span>
<span>sleep<span>
```

### 对象属性

`.`可以用于访问对象属性

输入：

```
SEX:{{msg.sex}};AGE:{{msg.age}}
```

输出：

```
SEX:male;AGE:66
```

### 块

块根据当前上下文中键的值呈现文本块零次或多次。

一个块以`#`开头，以`\`结尾。也就是说，`{{#subject}}`开始一个`subject`部分，同时`{{/subject}}`结束它。两个标签之间的文本称为该部分的“块”。

该部分的行为由键的值决定。

**如果`subject`键不存在，或存在且值为`null`, `undefined`, `false`, `0`, 或`NaN`, 或为空字符串或空列表，则不会渲染块。**

输入：

```
{{#msg}}SEX:{{sex}};AGE:{{age}};HOBBY:{{hobby}}{{/msg}}
MOREINFO:{{#moreInfo}}cannot display{{/moreInfo}}
```

输出：

```
SEX:male;AGE:66;HOBBY:reading
MOREINFO:
```

### 非空列表

如果`persons`键存在且不是`null`, `undefined`, 或`false`, 并且不是空列表，则该块将被渲染一次或多次。

当值是一个列表时，该块为列表中的每个项目呈现一次。每次迭代时，块的上下文都设置为列表中的当前项。通过这种方式，我们可以遍历集合。

data:

```
"persons": [
    { "name": "CAI" },
    { "name": "LIU" },
    { "name": "ZHOU" }
  ]
```

输入：

```
{{#persons}}
{{name}}
{{/persons}}
```

输出：

```
CAI
LIU
ZHOU
```

### 枚举

`.`可以遍历字符串数组

输入

```
{{#subject}}{{.}}{{/subject}}
```

输出

```
 Ch  En  Math  physics 
```

### 函数

解析的数据不仅可以为值，也可以是一个函数

data:

```
fun: function () {
    return 2 + 4;
 }
```

输入：

```
FUN:{{fun}}
```

输出

```
FUN:6
```

### if else

`{{^section}}`与`{{#section}}`相反。仅当该部分的标记的值为`null`, `undefined`, `false`, *falsy*或空列表时，才会呈现倒置部分的块。

if

输入：

```
EMPTY-ARR:{{#moreInfo}}empty-arr{{/moreInfo}}\nARRAY: {{#subject}}arr {{/subject}}\n
```

输出：

```
EMPTY-ARR:
ARRAY:arr arr arr arr
```

else

输入：

```
EMPTY-ARR:{{^moreInfo}}empty-arr{{/moreInfo}}\nARRAY: {{^subject}}arr {{/subject}}\n
```

输出：

```
EMPTY-ARR:empty-arr
ARRAY:
```

### 注释

以 `!` 开头会被忽略

输入：

```
{{!name}}.
```

输出：

```
 .
```

### 模块

`>`可以表示一个模块，把复杂的模板可以拆出多个简单的模块来便于使用

额外data:

```
template: "{{#msg}}SEX:{{sex}};AGE:{{age}};HOBBY:{{hobby}}{{/msg}}"
```

输入：

```
NAME:{{name}}\nINFO:\n{{>template}}
```

输出：

```
NAME:cai
INFO:
SEX:male;AGE:66;HOBBY:reading
```

### 自定义分隔符

可以通过修改Mustache的tags来自定义分隔符，也可以在解析时直接使用

输入

```
Mustache.render(NAME:{{name}}\nNAME:<%name%>\n, this.data, {}, ['<%', '%>'])
```

输出：

```
NAME:{{name}}
NAME:cai
```

### 预解析和缓存模板

默认情况下，当 mustache 第一次解析模板时，它会将解析保存在缓存中。下次它看到相同的模板时，它会跳过解析步骤并更快地呈现模板。如果您愿意，可以使用`Mustache.parse`.

## 接口说明

1. 解析模板`Mustache.render()`
2. 预解析:`Mustache.parse`
3. 清除默认编写器中的所有缓存模板：`mustache.clearCache()`

## 约束与限制
在下述版本验证通过：
- DevEco Studio 版本: 4.1 Canary(4.1.3.317), OpenHarmony SDK: API11（4.1.0.36）

- DevEco Studio: 4.0 Beta2(4.0.3.512), SDK: API10 (4.0.10.9)

- DevEco Studio: 3.1 Beta2(3.1.0.400), SDK: API9 Release(3.2.11.9)
## 目录结构

```
|---- Jmustache  
|     |---- entry  # 示例代码文件夹
|     |---- Jmustache  # Jmustache库文件夹
|           |---- index.ets  # 对外接口
|           |---- src
|                 |---- ets
|                       │---- components
|                             |---- mustache.ets # 用于解析模板
|     |---- README.md  # 安装使用方法                    
```

## 贡献代码

使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/jmustache/issues) 给我们，当然，我们也非常欢迎你给我们发[PR](https://gitee.com/openharmony-sig/jmustache/pulls) 。

## 开源协议

本项目基于 [Apache License](https://gitee.com/openharmony-sig/jmustache/blob/master/LICENSE) ，请自由地享受和参与开源。
